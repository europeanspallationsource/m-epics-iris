require asyn, 4.27.0
require streamdevice, 2.7.1
require tpmac, 3.11.2
require iris, 2.0.0


# Set environmental variables
epicsEnvSet("ASYN_PORT",    		"GEOBRICK_ASYN")
epicsEnvSet("PMAC_IP", 				"10.10.1.40")
epicsEnvSet("PMAC_PORT", 			"1025")


#fonctions from TPMAC
# Connection to GEOBRICK, create a asyn port
pmacAsynIPConfigure($(ASYN_PORT), $(PMAC_IP):$(PMAC_PORT))

dbLoadRecords("get_value_pmac.db")
dbLoadRecords("set_value_pmac.db")
dbLoadRecords("motor_iris.db")
dbLoadRecords("console.db")




