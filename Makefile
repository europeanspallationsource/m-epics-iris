include $(EPICS_ENV_PATH)/module.Makefile

#STARTUPS = startup/iris.cmd
DOC += doc/README.md
DOC += doc/Iris_control_system_documentation.pdf
MISCS += misc/source_conf.pmc
MISCS += misc/conf_2016-09-16.CFG
MISCS += misc/M_variables.pmc
MISCS += misc/compilated_conf.PMA
MISCS += protocol/pmacVariables.proto
#USR_DEPENDENCIES = <module>,<version>
USR_DEPENDENCIES = streamdevice,2.7.7
